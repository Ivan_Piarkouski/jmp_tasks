package by.epam.lambda2;

import by.epam.lambda2.entity.Author;
import by.epam.lambda2.entity.Book;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        Preparator preparator = new Preparator();
        Book[] books = preparator.getBooks();
        Author[] authors = preparator.getAuthors();

        Stream<Book> bookStream = Arrays.asList(books).stream();

        System.out.println("\nDoes any book have more than 200 pages:");
        System.out.println(Arrays.asList(books)
                .stream().
                        anyMatch(book -> book.getNumberOfPages() > 200));

        System.out.println("\nDo all books have more than 200 pages:");
        System.out.println(Arrays.asList(books)
                .stream()
                .allMatch(book -> book.getNumberOfPages() > 200));

        System.out.println("\nFilter books with only single author:");
        Arrays.asList(books)
                .stream()
                //Use parallel stream with subtask #3
                .parallel()
                .filter(book -> book.getAuthors().size() == 1)
                .forEach(System.out::println);

        System.out.println("\nSort the books by number of pages:");
        Arrays.asList(books)
                .stream()
                .sorted(Comparator.comparing(Book::getNumberOfPages))
                .forEach(book -> System.out.println(book.getNumberOfPages()));

        System.out.println("\nSort the books by title:");
        Arrays.asList(books)
                .stream()
                .sorted(Comparator.comparing(Book::getTitle))
                .forEach(book -> System.out.println(book.getTitle()));

        System.out.println("\nGet list of all titles and print them using forEach method:");
        List<String> titles = Arrays.asList(books)
                .stream()
                .map(book -> book.getTitle())
                .collect(Collectors.toList());
        titles.forEach(System.out::println);

        System.out.println("\nGet distinct list of all authors:");
        List<Author> distinctAuthors =  Arrays.asList(books)
                .stream()
                .flatMap(book -> book.getAuthors().stream())
                //Use peek method for debugging intermediate streams during execution the previous task.
                .peek(author -> System.out.println("DEBUG: " + author.getName())) //Use peek method for debugging intermediate streams during execution the previous task.
                .distinct()
                .collect(Collectors.toList());
        distinctAuthors.forEach(System.out :: println);

        System.out.println("\nUse the Option type for determining the title of the biggest book of some author:");
        Arrays.asList(authors)
                .stream()
                .forEach(author -> {Optional<Book> biggestBook =
                        author.getBooks()
                                .stream()
                                .max(Comparator.comparingInt(Book::getNumberOfPages));
                    System.out.println("the biggest book of author: " + author.getName() + " - "
                            + biggestBook.get().getTitle());
                });
    }
}
