package by.epam.lambda2;

import by.epam.lambda2.entity.Author;
import by.epam.lambda2.entity.Book;

import java.util.ArrayList;
import java.util.List;

/**
 * Prepares Books and Autors Arrays
 */
public class Preparator {

    private  Author[] authors;
    private  Book[] books;

    public Preparator(){
       authors = new Author[]{new Author("London", (short) 50, null),
               new Author("Scott", (short) 30, null),
               new Author("Pushkin", (short) 38, null),
               new Author("Esenin", (short) 78, null),
               new Author("Rid", (short) 45, null)};

       books = new Book[]{new Book("Fiction", 300, null),
               new Book("Tales", 150, null),
               new Book("Adventure", 250, null),
               new Book("Fairy Tales", 100, null),
               new Book("Smiley", 400, null)};

       List<Book> booksList1 = new ArrayList<>();
       booksList1.add(books[1]);
       booksList1.add(books[3]);
       booksList1.add(books[4]);
       authors[0].setBooks(booksList1);

       List <Book> booksList2 = new ArrayList<>();
       booksList2.add(books[0]);
       authors[1].setBooks(booksList2);

       List <Book> booksList3 = new ArrayList<>();
       booksList3.add(books[2]);
       booksList3.add(books[3]);
       authors[2].setBooks(booksList3);

       List <Book> booksList4 = new ArrayList<>();
       booksList4.add(books[3]);
       booksList4.add(books[4]);
       authors[3].setBooks(booksList4);

       List <Book> booksList5 = new ArrayList<>();
       booksList5.add(books[2]);
       booksList5.add(books[4]);
       authors[4].setBooks(booksList5);

       List<Author> authorList1 = new ArrayList<>();
       authorList1.add(authors[1]);
       books[0].setAuthors(authorList1);

       List<Author> authorList2 = new ArrayList<>();
       authorList2.add(authors[0]);
       books[1].setAuthors(authorList2);

       List<Author> authorList3 = new ArrayList<>();
       authorList3.add(authors[2]);
       authorList3.add(authors[4]);
       books[2].setAuthors(authorList3);

       List<Author> authorList4 = new ArrayList<>();
       authorList4.add(authors[0]);
       authorList4.add(authors[2]);
       authorList4.add(authors[3]);
       books[3].setAuthors(authorList4);

       List<Author> authorList5 = new ArrayList<>();
       authorList5.add(authors[0]);
       authorList5.add(authors[3]);
       authorList5.add(authors[4]);
       books[4].setAuthors(authorList5);
   }

    public Author[] getAuthors() {
        return authors;
    }

    public Book[] getBooks() {
        return books;
    }
}
