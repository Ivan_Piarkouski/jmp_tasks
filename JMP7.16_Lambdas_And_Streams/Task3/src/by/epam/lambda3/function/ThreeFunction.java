package by.epam.lambda3.function;

/**
 * 1. Write your own functional interface ThreeFunction (it takes three arguments and produce result).
 */
@FunctionalInterface
public interface ThreeFunction <T, U, Y, R>{

   R apply (T t, U u, Y y);
}
