package by.epam.lambda3;

import by.epam.lambda3.entity.Author;
import by.epam.lambda3.entity.Book;
import by.epam.lambda3.function.ThreeFunction;
import by.epam.lambda3.util.Preparator;

public class Main {

    public static void main(String[] args) {

        Preparator preparator = new Preparator();
        Book[] books = preparator.getBooks();
        Author[] authors = preparator.getAuthors();

        // 2. Implement your own functional interface ThreeFunction with two different lambdas
        ThreeFunction<Author, Author, Book, Boolean> hasAuthorsGivenBook=
                (author1, author2, book) ->
                        author1.getBooks().contains(book) && author2.getBooks().contains(book);

        ThreeFunction<Book, Book, Author, Boolean> hasBooksGivenAuthor=
                (book1, book2, author) ->
                        book1.getAuthors().contains(author) &&  book2.getAuthors().contains(author);

        // 3. Provide client code with usage of this lambdas
        System.out.println(hasAuthorsGivenBook.apply(authors[0],authors[2], books[0]));
        System.out.println(hasAuthorsGivenBook.apply(authors[0],authors[2], books[3]));

        System.out.println(hasBooksGivenAuthor.apply(books[2], books[4], authors[2]));
        System.out.println(hasBooksGivenAuthor.apply(books[2], books[4], authors[4]));
    }
}
