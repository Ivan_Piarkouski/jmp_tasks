package by.epam.lambda1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.*;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        runningThreadsWithLambdas();
        Thread.sleep(2000);
        sortPersonArray();
        implementStandartFunctionalInterfaces();
        useCustomInterface();
    }

    /** runs threads */
    private static void runThread(Runnable runnable)
    {
        new Thread(runnable).start();
    }
    /**
     * Create several instances of Runnable interface with different implementation using lambda expressions.
     * Use them with threads.
     */
    private static void runningThreadsWithLambdas() {

        System.out.println("\nTask : * Create several instances of Runnable interface with different implementation using lambda expressions.\n" +
                "     * Use them with threads.\n");
        Runnable runnableForThreadTwo = () ->{
            System.out.println("Thread two is runnung");
            System.out.println("Thread two starts sleeping");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread two is awake and finishes");
        };

        runThread(() -> {
            System.out.println("thread one is running");
            System.out.println(5 + 75);
        });
        runThread(runnableForThreadTwo);
    }

    /**
     * Create:
     * a Person class with name and age fields;
     * an array of Persons;
     * two instances of Comparator<Person> interface using lambda expressions: first one for comparing by name, second one – by age;
     * Then sort them using these comparators;
     * Use forEach method for printing information about all the persons. Use the method reference;
     */
    private static void sortPersonArray() {
        System.out.println("\n\nTask : * Create:\n" +
                "     * a Person class with name and age fields;\n" +
                "     * an array of Persons;\n" +
                "     * two instances of Comparator<Person> interface using lambda expressions: first one for comparing by name, second one – by age;\n" +
                "     * Then sort them using these comparators;\n" +
                "     * Use forEach method for printing information about all the persons. Use the method reference;\n" +
                "     *\n\n");

        Person [] people = {new Person("Sasha", 30), new Person("Pasha", 20),
                new Person("Dasha", 28), new Person("Masha", 35)};

        Comparator<Person> personComparatorByName = (Person person1, Person person2) -> {
            return person1.getName().compareTo(person2.getName());
        };
        Comparator<Person> personComparatorByAge = (Person person1, Person person2) -> {
            return person1.getAge()- person2.getAge();
        };

        System.out.println("Persons before sorting");
        Arrays.asList(people).forEach(person -> System.out.println(person));
        Arrays.sort(people, personComparatorByName);
        System.out.println("Persons after sorting by name");
        Arrays.asList(people).forEach(person -> System.out.println(person));
        Arrays.sort(people, personComparatorByAge);
        System.out.println("Persons after sorting by age");
        Arrays.asList(people).forEach(person -> System.out.println(person));

        //comparators can be rewritten using method reference and comparing
        personComparatorByName = Comparator.comparing(Person::getName);
        personComparatorByAge = Comparator.comparingInt(Person::getAge);
    }

    /**
     * Implement each of main Java Standard Library functional interfaces (supplier, predicate etc.)
     * using lambda expressions.
     */
    private static void implementStandartFunctionalInterfaces()
    {
        System.out.println("\n\nTask : * Implement each of main Java Standard Library functional interfaces (supplier, predicate etc.)\n" +
                "     * using lambda expressions.\n\n");

        //Runnable
        System.out.println("Runnable");
        Runnable runnable = () ->{
            System.out.println("Thread two is runnung");
        };
        runnable.run();

        //Consumer
        System.out.println("Consumer");
        Consumer<Person> personConsumer = (Person person) -> System.out.println(person);
        personConsumer.accept(new Person("Misha", 27));

        //Supplier
        System.out.println("Supplier");
        Supplier<Person> personSupplier = () -> new Person("Kesha", 15);
        System.out.println(personSupplier.get());

        //Function
        System.out.println("Function");
        Function<Person, String> personFunction = (person) -> person.getName();
        System.out.println(personFunction.apply(new Person("Peter", 40)));

        //BiFunction
        System.out.println("BiFunction");
        BiFunction<Person, Integer, Boolean> personBiFunction = (person, age) -> person.getAge() == age;
        System.out.println(personBiFunction.apply(new Person("Kate", 20), 20));

        //UnaryOperator
        System.out.println("UnaryOperator");
        UnaryOperator<Person> personUnaryOperator = (person) -> new Person(person.getName(), person.getAge());
        System.out.println(personUnaryOperator.apply(new Person("Roger", 55)));

        //Predicate
        System.out.println("Predicate");
        Predicate<Person> personPredicate = (person) -> person.getName().equals("Harry");
        System.out.println(personPredicate.test(new Person("Roger", 55)));
    }

    /**
     * Create your own functional interface and add several its implementations using both lambda expressions and inner anonymous classes.
     * Add few default methods to it and use them.
     * Add few static methods to it and use them.
     */
    private static void useCustomInterface()
    {
        System.out.println("\n\nTask : * Create your own functional interface and add several its implementations using both lambda expressions and inner anonymous classes.\n" +
                "     * Add few default methods to it and use them.\n" +
                "     * Add few static methods to it and use them.\n\n");
        CustomFunctionInterface<Person> updatePersonName = person -> person.setName(person.getName() + " Igorevich");
        CustomFunctionInterface<Person> updatePersonAge = person -> person.setAge(person.getAge() + 3);
        CustomFunctionInterface<Person> ereasePersonData = new CustomFunctionInterface<Person>() {
            @Override
            public void updateGivenObject(Person person) {
                person.setAge(0);
                person.setName("");
            }
        };

        CustomFunctionInterface.announcement();

        Person person = new Person("Ivan", 30);
        System.out.println(person);
        updatePersonName.updateGivenObject(person);
        updatePersonName.printLn(person);
        updatePersonAge.updateGivenObject(person);
        updatePersonAge.printLn(person);
        ereasePersonData.updateGivenObject(person);
        ereasePersonData.printLn(person);
    }
}
