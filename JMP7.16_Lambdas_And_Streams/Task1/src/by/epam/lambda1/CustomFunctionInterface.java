package by.epam.lambda1;

/**
 * Custom functional interface
 */
@FunctionalInterface
public interface CustomFunctionInterface <T> {

    static void announcement(){
        System.out.println("this is ststic method of CustomFunctionInterface");
    }

    /**
     *  default method
     * @param t
     */
    default void printLn(T t)
    {
        System.out.println(t);
    }

    void updateGivenObject(T t);
}
