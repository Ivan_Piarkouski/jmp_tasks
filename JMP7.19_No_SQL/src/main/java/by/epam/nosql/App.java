package by.epam.nosql;

import by.epam.nosql.dao.MongoDao;
import by.epam.nosql.dao.entity.Note;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 * Mongo app
 */
public class App {

    private static void insertNote(Scanner scanner, MongoDao mongoDao) {
        scanner.nextLine();
        System.out.println("Please enter tag name");
        String tagName = scanner.nextLine();
        System.out.println("Please enter text");
        String text = scanner.nextLine();

        Note note = new Note();
        note.setCreationDate(new Date());
        note.setTag(tagName);
        note.setText(text);

        mongoDao.add(note);
    }

    private static void seeNotes(MongoDao mongoDao) {
        List<Note> noteList = mongoDao.getAllNotes();
        noteList.forEach(System.out::println);
    }

    private static void deleteCase(Scanner scanner, MongoDao mongoDao, List<Note> noteList) {
        System.out.println("Do you want to delete found items? - insert 1 for 'yes' and 2 for 'no'");
        int select = scanner.nextInt();
        if (select == 1) {
            mongoDao.deleteList(noteList);
        }
    }

    private static void seeNotesByTag(Scanner scanner, MongoDao mongoDao) {
        scanner.nextLine();
        System.out.println("Please enter tag name");
        String tagName = scanner.nextLine();
        List<Note> noteList = mongoDao.getNotesByTag(tagName);
        noteList.forEach(System.out::println);
        deleteCase(scanner, mongoDao, noteList);
    }

    private static void seeNotesByTagAndText(Scanner scanner, MongoDao mongoDao) {
        scanner.nextLine();
        System.out.println("Please enter tag name");
        String tagName = scanner.nextLine();
        System.out.println("Please enter text");
        String text = scanner.nextLine();
        List<Note> noteList = mongoDao.getNotesByTagAndText(tagName, text);
        noteList.forEach(System.out::println);
        deleteCase(scanner, mongoDao, noteList);
    }

    public static void main(String[] args) {
        MongoDao mongoDao = new MongoDao();
        Scanner scanner = new Scanner(System.in);
        int select = 0;
        while (select != 5) {
            System.out.println("Please select menu item");
            System.out.println("1-create note, 2-see notes, 3-see notes by tag, 4-see notes by tag and text, 5-exit");
            select = scanner.nextInt();

            switch (select) {
                case 1:
                    insertNote(scanner, mongoDao);
                    break;
                case 2:
                    seeNotes(mongoDao);
                    break;
                case 3:
                    seeNotesByTag(scanner, mongoDao);
                    break;
                case 4:
                    seeNotesByTagAndText(scanner, mongoDao);
                    break;
                default:
                    break;
            }
        }
        scanner.close();
    }


}
