package by.epam.nosql.dao.entity;

import java.util.Date;

/**
 * Note
 */
public class Note {

    private Date creationDate;
    private String tag;
    private String text;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Note note = (Note) o;

        if (creationDate != null ? !creationDate.equals(note.creationDate) : note.creationDate != null) return false;
        if (tag != null ? !tag.equals(note.tag) : note.tag != null) return false;
        return text != null ? text.equals(note.text) : note.text == null;
    }

    @Override
    public int hashCode() {
        int result = creationDate != null ? creationDate.hashCode() : 0;
        result = 31 * result + (tag != null ? tag.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Note{" +
                "creationDate=" + creationDate +
                ", tag='" + tag + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
