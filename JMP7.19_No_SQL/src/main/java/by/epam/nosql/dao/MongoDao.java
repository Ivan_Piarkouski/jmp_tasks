package by.epam.nosql.dao;

import by.epam.nosql.dao.entity.Note;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Mongo Dao
 */
public class MongoDao {

    private MongoClient mongoClient;
    private MongoDatabase mongoDB;
    private MongoCollection<Document> notes;

    public MongoDao() {
        mongoClient = new MongoClient();
        mongoDB = mongoClient.getDatabase("local");
        notes = mongoDB.getCollection("Notes");
    }

    private Document createDocument(Note note) {
        Document noteDoc = new Document();
        noteDoc.put("creationDate", note.getCreationDate());
        noteDoc.put("tag", note.getTag());
        noteDoc.put("text", note.getText());
        return noteDoc;
    }

    public void add(Note note) {
        Document noteDoc = createDocument(note);
        notes.insertOne(noteDoc);
    }

    private List<Note> fillList(List<Note> noteList, FindIterable<Document> results) {
        results.forEach((Block<? super Document>) (noteDoc) -> {
            Note note = new Note();
            note.setCreationDate(noteDoc.getDate("creationDate"));
            note.setTag(noteDoc.getString("tag"));
            note.setText(noteDoc.getString("text"));
            noteList.add(note);
        });
        return noteList;
    }

    public List<Note> getAllNotes() {
        List<Note> noteList = new ArrayList<Note>();
        FindIterable<Document> results = notes.find();
        noteList = fillList(noteList, results);

        return noteList;
    }

    public List<Note> getNotesByTag(String tag) {
        List<Note> noteList = new ArrayList<Note>();
        FindIterable<Document> results = notes.find(new Document("tag", tag));
        noteList = fillList(noteList, results);

        return noteList;
    }

    public List<Note> getNotesByTagAndText(String tag, String text) {
        List<Note> noteList = new ArrayList<Note>();
        FindIterable<Document> results = notes.find(new Document("tag", tag).append("text", text));
        noteList = fillList(noteList, results);

        return noteList;
    }

    public void deleteList(List<Note> noteList) {
        noteList.forEach((note -> {
            Document noteDoc = createDocument(note);
            notes.deleteMany(noteDoc);
        }));
    }
}
