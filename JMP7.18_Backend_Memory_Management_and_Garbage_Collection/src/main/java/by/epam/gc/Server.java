package by.epam.gc;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

public class Server {

    private static final Set<String> cache = new HashSet<>();
    //single list of buffers
    private static final List<byte[]> LIST_OF_BUFFERS = Arrays.asList(new byte[1004], new byte[1005],new byte[1006],
            new byte[1007],new byte[1008],new byte[1009],new byte[1010],new byte[1011],new byte[1012],new byte[1013]);

    private int port;
    private ServerSocket server;
    private Socket socket;
    private DataOutputStream os;
    private DataInputStream is;
    private ReentrantLock lock = new ReentrantLock();

    public Server(int port) {
        this.port = port;
    }

    public void start() throws IOException {
        server = new ServerSocket(port);
        socket = server.accept();
        os = new DataOutputStream(socket.getOutputStream());
        is = new DataInputStream(socket.getInputStream());

        while (true) {
            readMessage();
        }
    }

    //method resolving buffers
   private byte[] getStringBuffer(int length)
   {
        for (byte[] buffer : LIST_OF_BUFFERS)
        {
            if (length == buffer.length){
                return buffer;
            }
        }
        throw new RuntimeException("No buffer was found for number " + length);
   }

    private void readMessage() throws IOException {
        int count = is.readInt();
        for (int i = 0; i < count; i++) {
            int length = is.readInt();
            byte[] bytes = getStringBuffer(length + 4); //added a method resolving Buffers (byte arrays) and single List of Buffers
            is.readFully(bytes);
            int index = i;
            calcSentence(bytes, index); //removed thread creation to save memory
        }
    }

    private void calcSentence(byte[] bytes, int index) {
        String str = String.valueOf(bytes).intern(); //removed string creation with new
        int big = 0;
        int small = 0;
        for (String word : str.split(" ")) {
            if (word.length() <= 4) {
                cache.add(word);
                small++;
            } else {
                big++;
            }
        }
        try {
            lock.lock();
            os.writeInt(index);
            os.writeInt(big);
            os.writeInt(small);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) throws IOException {
        new Server(7890).start();
    }
}
