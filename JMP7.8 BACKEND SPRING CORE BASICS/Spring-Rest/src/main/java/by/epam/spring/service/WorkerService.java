package by.epam.spring.service;

import by.epam.spring.dao.WorkerDao;
import by.epam.spring.dao.WorkerDaoImpl;
import by.epam.spring.resource.Worker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Rest Service.
 */
@Component
@Scope("request")
@Path("/")
public class WorkerService
{
    @Autowired
    /** workers dao */
    private WorkerDao workerDao;

    @GET
    @Path("/workers")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Worker> getWorkers()
    {
        return workerDao.getAllWorkers();
    }

    @GET
    @Path("/worker/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getWorker(@PathParam("id") Integer id)
    {
        Worker worker = workerDao.getWorker(id);
        Response response;
        if(worker == null)
        {
            response = Response.status(Response.Status.NOT_FOUND).entity("Worker not found").build();
        }
        else
        {
            response = Response.ok(worker).build();
        }
        return response;
    }

    @POST
    @Path("/worker")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response updateWorker(Worker worker)
    {
        Worker oldWorker = workerDao.getWorker(worker.getId());
        Response response;
        if(oldWorker == null)
        {
            response = Response.status(Response.Status.NOT_FOUND).entity("Worker not found").build();
        }
        else
        {
            workerDao.updateWorker(worker);
            response = Response.ok("Worker updated").build();
        }
        return response;
    }


    @PUT
    @Path("/worker")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response addWorker(Worker worker)
    {
        workerDao.addWorker(worker);
        return Response.ok("Worker added").build();
    }

    @DELETE
    @Path("/worker/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteWorker(@PathParam("id") Integer id)
    {
        Worker deleteCandidate = workerDao.getWorker(id);
        Response response;
        if(deleteCandidate == null)
        {
            response = Response.status(Response.Status.NOT_FOUND).entity("Worker not found").build();
        }
        else
        {
            response = Response.ok("Worker deleted").build();
            workerDao.deleteWorker(deleteCandidate);
        }
        return response;
    }
}
