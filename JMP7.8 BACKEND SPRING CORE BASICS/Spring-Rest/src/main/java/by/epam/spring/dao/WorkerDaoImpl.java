package by.epam.spring.dao;

import by.epam.spring.resource.Worker;

import java.util.*;

/**
 * A class containing workers
 */
public class WorkerDaoImpl implements WorkerDao
{

    /** sequence to set id */
    private  Integer workersSequence;
    /** workers map */
    private Map<Integer, Worker> workers;

    /**
     * constructs worker dao
     */
    public WorkerDaoImpl()
    {
        this.workers = new HashMap<Integer, Worker>();
        workersSequence = workers.size() - 1;
    }

    /**
     * constructs worker dao
     */
    public WorkerDaoImpl(Map<Integer, Worker> workers)
    {
        this.workers = workers;
        workersSequence = workers.size() - 1;
    }

    /**
     * returns workers list
     * @return workers
     */
    public List<Worker> getAllWorkers()
    {
        return new ArrayList<Worker>(workers.values());
    }

    /**
     * returns worker with specified id or null
     * @param id workerId
     * @return worker
     */
    public Worker getWorker(Integer id)
    {
        return workers.get(id);
    }

    /**
     * updates given worker
     * @param worker worker
     */
    public synchronized void updateWorker(Worker worker)
    {
        workers.put(worker.getId(), worker);
    }

    /**
     * adds specified worker
     * @param worker worker
     */
    public synchronized void addWorker(Worker worker)
    {
        workersSequence = ++workersSequence;
        worker.setId(workersSequence);
        workers.put(workersSequence, worker);
    }

    /**
     * deletes specified worker
     * @param worker
     */
    public synchronized void deleteWorker (Worker worker)
    {
        workers.remove(worker.getId());
    }

}
