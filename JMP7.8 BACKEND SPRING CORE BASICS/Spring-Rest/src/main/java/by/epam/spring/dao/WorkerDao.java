package by.epam.spring.dao;

import by.epam.spring.resource.Worker;

import java.util.List;

/**
 * Interface for Dao
 */
public interface WorkerDao
{
    /**
     * returns workers list
     * @return workers
     */
    List<Worker> getAllWorkers();

    /**
     * returns worker with specified id or null
     * @param id workerId
     * @return worker
     */
    Worker getWorker(Integer id);

    /**
     * updates given worker
     * @param worker worker
     */
    void updateWorker(Worker worker);

    /**
     * adds specified worker
     * @param worker worker
     */
    void addWorker(Worker worker);

    /**
     * deletes specified worker
     * @param worker
     */
    void deleteWorker (Worker worker);
}
