package by.epam.jmp.web.controller;

import by.epam.jmp.web.exception.PageNotFoundException;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Exception Hanling Controller
 */
@ControllerAdvice
public class ExceptionHanlingController {

    private static Logger logger = Logger.getLogger(ExceptionHanlingController.class);

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView handleError(HttpServletRequest req, Exception ex)
    {
        logger.error("Request: " + req.getRequestURL() + " raised " + ex);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("exception", ex);
        modelAndView.addObject("url", req.getRequestURL());
        modelAndView.setViewName("error");
        return modelAndView;
    }

    @ExceptionHandler(PageNotFoundException.class)
    @ResponseStatus(value= HttpStatus.NOT_FOUND)
    public String pageNotFound (HttpServletRequest req)
    {
        logger.error("Page not found for url: " + req.getRequestURL());

        return "pageNotFound";
    }
}
