package by.epam.jmp.web.config;

import by.epam.jmp.web.filter.EncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;

/**
 * Replacement of web.xml config
 */
public class WebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer
{
    @Override
    protected Filter[] getServletFilters() {
        return new Filter[] { new EncodingFilter() };
    }

    protected Class<?>[] getRootConfigClasses()
    {
        return new Class [] {WebConfig.class};
    }

    protected Class<?>[] getServletConfigClasses()
    {
        return null;
    }

    protected String[] getServletMappings()
    {
        return new String[] {"/"};
    }
}
