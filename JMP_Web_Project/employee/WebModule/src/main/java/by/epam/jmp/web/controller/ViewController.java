package by.epam.jmp.web.controller;

import by.epam.jmp.service.IEmployeeService;
import by.epam.jmp.service.dto.EmployeeVM;
import by.epam.jmp.web.exception.PageNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

/**
 * Url mapping controller
 */
@Controller
@RequestMapping("/")
public class ViewController {

    private final static String MODEL_ATTRIBUTE_NAME = "employeeVM";

    @Autowired
    private IEmployeeService employeeService;

    @RequestMapping(value = { "/"}, method = RequestMethod.GET)
    public String employeeList(ModelMap model)
    {
        List<EmployeeVM> employeeVMList = employeeService.findAll();
        model.put("employeeList", employeeVMList);
        return "employeeList";
    }

    @RequestMapping(value = { "/{id}"}, method = RequestMethod.GET)
    public String employee(@PathVariable Long id, ModelMap model)
    {
        EmployeeVM employeeVM = employeeService.find(id);
        if(employeeVM == null)
        {
            throw new PageNotFoundException("Page not found for emloyee with id "+ id);
        }
        model.put("employee", employeeVM);
        return "employee";
    }

    @RequestMapping(value = { "/addEmployee"}, method = RequestMethod.GET)
    public String addEmployeeForm(ModelMap model)
    {
        List<String> roles = employeeService.getRoleList();
        List<String> levels = employeeService.getLevelList();
        EmployeeVM employeeVM = new EmployeeVM();
        model.put("roles", roles);
        model.put("levels", levels);
        model.put(MODEL_ATTRIBUTE_NAME, employeeVM);
        return "addEmployeeForm";
    }

    @RequestMapping(value = { "/addEmployee"}, method = RequestMethod.POST)
    public String addEmployee(@Valid @ModelAttribute(MODEL_ATTRIBUTE_NAME)EmployeeVM employeeVM,
                              BindingResult bindingResult, ModelMap model)
    {
        if (bindingResult.hasErrors())
        {
            List<String> roles = employeeService.getRoleList();
            List<String> levels = employeeService.getLevelList();
            model.put("roles", roles);
            model.put("levels", levels);
            return "addEmployeeForm";
        }
        employeeService.save(employeeVM);
        return "redirect:/";
    }

    @RequestMapping(value = { "delete/{id}"}, method = RequestMethod.POST)
    public String deleteEmployee(@PathVariable Long id)
    {
        employeeService.delete(id);
        return "redirect:/";
    }

    @RequestMapping(value = { "updateEmployee/{id}"}, method = RequestMethod.GET)
    public String updateEmployeeForm(@PathVariable Long id, ModelMap model)
    {
        EmployeeVM employeeVM = employeeService.find(id);
        if(employeeVM == null)
        {
            throw new PageNotFoundException("Page not found for emloyee with id "+ id);
        }
        List<String> roles = employeeService.getRoleList();
        List<String> levels = employeeService.getLevelList();
        model.put("roles", roles);
        model.put("levels", levels);
        model.put(MODEL_ATTRIBUTE_NAME, employeeVM);
        return "updateEmployeeForm";
    }

    @RequestMapping(value = { "updateEmployee"}, method = RequestMethod.POST)
    public String updateEmployee(@Valid @ModelAttribute(MODEL_ATTRIBUTE_NAME)EmployeeVM employeeVM,
                                 BindingResult bindingResult, ModelMap model)
    {
        if (bindingResult.hasErrors())
        {
            List<String> roles = employeeService.getRoleList();
            List<String> levels = employeeService.getLevelList();
            model.put("roles", roles);
            model.put("levels", levels);
            return "updateEmployeeForm";
        }
        employeeService.update(employeeVM);
        return "redirect:/" + employeeVM.getId();
    }
}
