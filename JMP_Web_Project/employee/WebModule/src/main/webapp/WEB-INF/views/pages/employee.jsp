<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
    <dl>
        <dt><b><spring:message code="property.id"/></b></dt>
        <dd>${employee.id}</dd>

        <dt><b><spring:message code="property.firstName"/></b></dt>
        <dd>${employee.firstName}</dd>

        <dt><b><spring:message code="property.lastName"/></b></dt>
        <dd>${employee.lastName}</dd>

        <dt><b><spring:message code="property.level"/></b></dt>
        <dd>${employee.level}</dd>

        <dt><b><spring:message code="property.role"/></b></dt>
        <dd>${employee.role}</dd>

        <dt><b><spring:message code="property.salary"/></b></dt>
        <dd>${employee.salary}</dd>
    </dl>
    <form id="fakeF"></form>
    <button form="fakeF" type="submit" formmethod="post"
    formaction="${pageContext.request.contextPath}/delete/${employee.id}">
    <spring:message code="button.delete"/></button>
    <button form="fakeF" type="submit" formmethod="get"
    formaction="${pageContext.request.contextPath}/updateEmployee/${employee.id}">
    <spring:message code="button.update"/></button>