<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<nav>
    <ul id="menu">
        <li><a href="${pageContext.request.contextPath}/"><spring:message code="menu.home"/></a></li>
        <li><a href="${pageContext.request.contextPath}/addEmployee"><spring:message code="menu.addEmloyee"/></a></li>
    </ul>
</nav>