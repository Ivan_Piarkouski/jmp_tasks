<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div>
        <h1><spring:message code="header.text"/></h1>
        <br>
       <p> <a href="?lang=en"><spring:message code="language.en"/></a> |
               <a href="?lang=ru"><spring:message code="language.ru"/></a> </p>
</div>