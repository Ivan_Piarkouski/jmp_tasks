<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
 <table class="table">
    <thead>
      <tr>
        <th><spring:message code="property.id"/></th>
        <th><spring:message code="property.firstName"/></th>
        <th><spring:message code="property.lastName"/></th>
        <th><spring:message code="property.level"/></th>
        <th><spring:message code="property.role"/></th>
        <th><spring:message code="property.salary"/></th>
      </tr>
    </thead>
    <tbody>
    	<c:forEach items="${employeeList}" var="employee">
    		  <tr>
        		<td><a href="${pageContext.request.contextPath}/${employee.id}">
        		${employee.id}</a></td>
        		<td><a href="${pageContext.request.contextPath}/${employee.id}">
        		${employee.firstName}</a></td>
        		<td><a href="${pageContext.request.contextPath}/${employee.id}">
        		${employee.lastName}</a></td>
        		<td>${employee.level}</td>
        		<td>${employee.role}</td>
        		<td>${employee.salary}</td>
      		  </tr>
		</c:forEach>
    </tbody>
  </table>