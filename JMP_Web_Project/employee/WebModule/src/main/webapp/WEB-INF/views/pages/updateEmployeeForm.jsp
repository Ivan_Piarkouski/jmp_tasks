<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<h3><spring:message code="add.employee.summary"/></h3>
<form:form method="POST" action="${pageContext.request.contextPath}/updateEmployee"
    modelAttribute="employeeVM">
    <dl>
        <form:input path="id" type="hidden"/>
        <dl>
            <dt><form:label path="firstName"><spring:message code="property.firstName"/></form:label></dt>
            <dd><form:input path="firstName"/> <form:errors path="firstName" cssClass="error"/> </dd>
        </dl>
        <dl>
            <dt><form:label path="lastName"><spring:message code="property.lastName"/></form:label></dt>
            <dd><form:input path="lastName"/> <form:errors path="lastName" cssClass="error"/> </dd>
        </dl>
        <dl>
            <dt><form:label path="level"><spring:message code="property.level"/></form:label></dt>
            <dd><form:select path="level" items="${levels}"/> <form:errors path="level" cssClass="error"/> </dd>
        </dl>
        <dl>
            <dt><form:label path="role"><spring:message code="property.role"/></form:label></dt>
            <dd><form:select path="role" items="${roles}"/> <form:errors path="role" cssClass="error"/> </dd>
        </dl>
        <dl>
            <dt><form:label path="salary"><spring:message code="property.salary"/></form:label></dt>
            <dd><form:input path="salary"/> <form:errors path="salary" cssClass="error"/> </dd>
        </dl>

        <dl>
            <li><input type="submit" value="<spring:message code="button.update"/>"/></li>
        </dl>
    </dl>
</form:form>