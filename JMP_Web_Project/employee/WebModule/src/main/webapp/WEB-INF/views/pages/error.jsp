<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

  <p><spring:message code="errorpage.message"/></p>

  <!--
    Failed URL: ${url}
    Exception:  ${exception.message}
        <c:forEach items="${exception.stackTrace}" var="ste">    ${ste}
    </c:forEach>
  -->