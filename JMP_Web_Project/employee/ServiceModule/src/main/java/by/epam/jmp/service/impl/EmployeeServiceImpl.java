package by.epam.jmp.service.impl;

import by.epam.jmp.persist.IEmployeeDao;
import by.epam.jmp.persist.entity.Employee;
import by.epam.jmp.persist.entity.Level;
import by.epam.jmp.persist.entity.Role;
import by.epam.jmp.service.IEmployeeService;
import by.epam.jmp.service.dto.EmployeeVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


/**
 * Employee Service Implementation
 */
@Component
@Transactional
public class EmployeeServiceImpl implements IEmployeeService{

    @Autowired
    private IEmployeeDao employeeDao;

    /**
     * creates Employee entity based on given EmployeeVM
     *
     * @param employeeVM - not Null
     * @return employee
     */
    private Employee createEmployee(EmployeeVM employeeVM)
    {
        Employee employee = new Employee();
        employee.setId(employeeVM.getId());
        employee.setFirstName(employeeVM.getFirstName());
        employee.setLastName(employeeVM.getLastName());
        employee.setLevel(Level.valueOf(employeeVM.getLevel()));
        employee.setRole(Role.valueOf(employeeVM.getRole()));
        employee.setSalary(employeeVM.getSalary());
        return employee;
    }

    /**
     * creates EmployeeVM dto based on given Employee entity
     *
     * @param employee - not Null
     * @return employeeVM
     */
    private EmployeeVM createEmployeeVM(Employee employee)
    {
        EmployeeVM employeeVM = null;
        if(employee != null)
        {
            employeeVM = new EmployeeVM();
            employeeVM.setId(employee.getId());
            employeeVM.setFirstName(employee.getFirstName());
            employeeVM.setLastName(employee.getLastName());
            employeeVM.setSalary(employee.getSalary());
            employeeVM.setLevel(employee.getLevel().toString());
            employeeVM.setRole(employee.getRole().toString());
        }
        return employeeVM;
    }

    /**
     * saves given view model
     *
     * @param employeeVM - not Null
     */
    public void save(EmployeeVM employeeVM)
    {
        employeeDao.save(createEmployee(employeeVM));
    }

    /**
     * updates given view model
     *
     * @param employeeVM - not Null
     * @return EmployeeVM employeeVM
     */
    public EmployeeVM update(EmployeeVM employeeVM)
    {
        return createEmployeeVM(employeeDao.update(createEmployee(employeeVM)));
    }

    /**
     * Deletes view model with given Id
     *
     * @param id - not Null
     */
    public void delete(Long id) {
        employeeDao.delete(id);
    }

    /**
     * find view model with given Id
     *
     * @param id - not Null
     * @return EmployeeVM employeeVM
     */
    public EmployeeVM find(Long id) {
        return createEmployeeVM(employeeDao.find(id));
    }

    /**
     * finds all view model
     *
     * @return List<EmployeeVM>
     */
    public List<EmployeeVM> findAll() {
        List<Employee> employees = employeeDao.findAll();
        List<EmployeeVM> employeeVMList = new ArrayList<EmployeeVM>(employees.size());
        for(Employee employee : employees)
        {
            employeeVMList.add(createEmployeeVM(employee));
        }
        return employeeVMList;
    }

    /**
     * method returns list of possible role names
     *
     * @return List<String> roleList
     */
    public List<String> getRoleList() {
        List<String> roleNames = new ArrayList<String>(Role.values().length);
        for(Role role : Role.values())
        {
            roleNames.add(role.name());
        }
        return roleNames;
    }

    /**
     * method returns list of possible level names
     *
     * @return List<String> levelList
     */
    public List<String> getLevelList() {
        List<String> levelNames = new ArrayList<String>(Level.values().length);
        for(Level level : Level.values())
        {
            levelNames.add(level.name());
        }
        return levelNames;
    }
}
