package by.epam.jmp.service;

import java.util.List;

/**
 * Base interface for Service classes
 */
public interface IBaseService <T>
{
    /**
     * saves given view model
     *
     * @param t - not Null
     */
    void save (T t);

    /**
     * updates given view model
     *
     * @param t - not Null
     * @return T t
     */
    T update (T t);

    /**
     * Deletes view model with given Id
     *
     * @param id - not Null
     */
    void delete (Long id);

    /**
     * find view model with given Id
     *
     * @param id - not Null
     * @return T t
     */
    T find (Long id);

    /**
     * finds all view model
     *
     * @return  List<T>
     */
    List<T> findAll();

}
