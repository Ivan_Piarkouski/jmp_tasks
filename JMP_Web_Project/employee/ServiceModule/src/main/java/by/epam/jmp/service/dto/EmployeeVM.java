package by.epam.jmp.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * View model for Employee entity
 */
public class EmployeeVM implements Serializable{

    private Long id;
    @NotNull
    @Size(min=2, max=255)
    private String firstName;
    @NotNull
    @Size(min=2, max=255)
    private String lastName;
    @NotNull
    @Pattern(regexp="(L1)|(L2)|(L3)|(L4)")
    private String level;
    @NotNull
    @Pattern(regexp="(DEVELOPER)|(TESTER)|(BUSINESS_ANALIST)|(PRODUCT_OWNER)|(PROJECT_MANAGER)|(ADMINISTRATIVE)")
    private String role;
    @NotNull
    @Min(1)
    @Max(50000)
    private Integer salary;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmployeeVM that = (EmployeeVM) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (level != null ? !level.equals(that.level) : that.level != null) return false;
        if (role != null ? !role.equals(that.role) : that.role != null) return false;
        return salary != null ? salary.equals(that.salary) : that.salary == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (level != null ? level.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + (salary != null ? salary.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EmployeeVM{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", level='" + level + '\'' +
                ", role='" + role + '\'' +
                ", salary=" + salary +
                '}';
    }
}
