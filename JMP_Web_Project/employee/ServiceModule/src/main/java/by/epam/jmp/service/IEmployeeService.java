package by.epam.jmp.service;

import by.epam.jmp.service.dto.EmployeeVM;

import java.util.List;

/**
 * Interface for Employee Service
 */
public interface IEmployeeService extends IBaseService <EmployeeVM>
{
    /**
     * method returns list of possible role names
     *
     * @return List<String> roleList
     */
    List<String> getRoleList();

    /**
     * method returns list of possible level names
     *
     * @return List<String> levelList
     */
    List<String> getLevelList();
}
