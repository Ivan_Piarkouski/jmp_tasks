package by.epam.jmp.service.config;

import by.epam.jmp.persist.config.PersistenceConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Config for Service module
 */
@Configuration
@ComponentScan("by.epam.jmp.service.impl")
@EnableTransactionManagement
@Import(PersistenceConfig.class)
public class ServiceModuleConfig {

}
