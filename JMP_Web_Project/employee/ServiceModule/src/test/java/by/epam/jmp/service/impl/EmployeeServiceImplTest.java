package by.epam.jmp.service.impl;

import by.epam.jmp.persist.IEmployeeDao;
import by.epam.jmp.persist.entity.Employee;
import by.epam.jmp.persist.entity.Level;
import by.epam.jmp.persist.entity.Role;
import by.epam.jmp.service.dto.EmployeeVM;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 * EmployeeServiceImpl Test
 */
@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceImplTest
{
    @InjectMocks
    private EmployeeServiceImpl employeeService;

    @Mock
    private IEmployeeDao employeeDao;

    private Employee createEmployee (EmployeeVM employeeVM)
    {
        Employee employee = new Employee();
        employee.setFirstName(employeeVM.getFirstName());
        employee.setLastName(employeeVM.getLastName());
        employee.setSalary(employeeVM.getSalary());
        employee.setLevel(Level.valueOf(employeeVM.getLevel()));
        employee.setRole(Role.valueOf(employeeVM.getRole()));
        employee.setId(employeeVM.getId());
        return employee;
    }

    @Test
    public void testSave()
    {
        EmployeeVM employeeVM = new EmployeeVM();
        employeeVM.setFirstName("firstName");
        employeeVM.setLastName("lastName");
        employeeVM.setLevel("L1");
        employeeVM.setRole("DEVELOPER");
        employeeVM.setSalary(500);

        Employee employee = createEmployee(employeeVM);

        employeeService.save(employeeVM);
        verify(employeeDao).save(employee);
    }

    @Test
    public void testUpdate()
    {
        EmployeeVM employeeVM = new EmployeeVM();
        employeeVM.setFirstName("firstName1");
        employeeVM.setLastName("lastName1");
        employeeVM.setLevel("L2");
        employeeVM.setRole("TESTER");
        employeeVM.setSalary(400);
        employeeVM.setId(10L);

        Employee employee = createEmployee(employeeVM);

        when(employeeDao.update(employee)).thenReturn(employee);
        EmployeeVM updatedEmployeeVM = employeeService.update(employeeVM);
        assertEquals(employeeVM, updatedEmployeeVM);
    }

    @Test
    public void testDelete()
    {
        Long idToDelete = 15L;
        employeeService.delete(idToDelete);
        verify(employeeDao).delete(idToDelete);
    }

    @Test
    public void testFind()
    {
        EmployeeVM employeeVM = new EmployeeVM();
        employeeVM.setFirstName("firstName1");
        employeeVM.setLastName("lastName1");
        employeeVM.setLevel("L2");
        employeeVM.setRole("TESTER");
        employeeVM.setSalary(400);
        employeeVM.setId(10L);

        Employee employee = createEmployee(employeeVM);

        when(employeeDao.find(employeeVM.getId())).thenReturn(employee);
        EmployeeVM foundEmployeeVM = employeeService.find(10L);
        assertEquals(employeeVM, foundEmployeeVM);
    }

    @Test
    public void testFindAll()
    {
        EmployeeVM employeeVM1 = new EmployeeVM();
        employeeVM1.setFirstName("firstName1");
        employeeVM1.setLastName("lastName1");
        employeeVM1.setLevel("L2");
        employeeVM1.setRole("TESTER");
        employeeVM1.setSalary(800);
        employeeVM1.setId(10L);

        EmployeeVM employeeVM2 = new EmployeeVM();
        employeeVM2.setFirstName("firstName2");
        employeeVM2.setLastName("lastName2");
        employeeVM2.setLevel("L4");
        employeeVM2.setRole("PRODUCT_OWNER");
        employeeVM2.setSalary(3000);
        employeeVM2.setId(10L);

        EmployeeVM employeeVM3 = new EmployeeVM();
        employeeVM3.setFirstName("firstName3");
        employeeVM3.setLastName("lastName3");
        employeeVM3.setLevel("L3");
        employeeVM3.setRole("DEVELOPER");
        employeeVM3.setSalary(1500);
        employeeVM3.setId(10L);

        List<EmployeeVM> employeeVMList = new ArrayList<EmployeeVM>();
        employeeVMList.add(employeeVM1);
        employeeVMList.add(employeeVM2);
        employeeVMList.add(employeeVM3);

        List<Employee> employeeList = new ArrayList<Employee>();
        employeeList.add(createEmployee(employeeVM1));
        employeeList.add(createEmployee(employeeVM2));
        employeeList.add(createEmployee(employeeVM3));

        when(employeeDao.findAll()).thenReturn(employeeList);
        List<EmployeeVM> foundEmployeeVMList = employeeService.findAll();

        assertEquals(employeeVMList, foundEmployeeVMList);
    }

    @Test
    public void testGetRoleList()
    {
        List<String> expectedRoleNames = new ArrayList<String>(Role.values().length);
        for(Role role : Role.values())
        {
            expectedRoleNames.add(role.name());
        }
        List<String> actualRoleNames = employeeService.getRoleList();
        assertEquals(expectedRoleNames,actualRoleNames);
    }

    @Test
    public void testGetLevelList()
    {
        List<String> expectedLevelNames = new ArrayList<String>(Level.values().length);
        for(Level level : Level.values())
        {
            expectedLevelNames.add(level.name());
        }
        List<String> actualLevelNames = employeeService.getLevelList();
        assertEquals(expectedLevelNames,actualLevelNames);
    }
}
