package by.epam.jmp.persist.entity;

/**
 * Enum for employee roles
 */
public enum Role {
    DEVELOPER, TESTER, BUSINESS_ANALIST, PRODUCT_OWNER, PROJECT_MANAGER, ADMINISTRATIVE
}
