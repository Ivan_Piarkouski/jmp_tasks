package by.epam.jmp.persist.impl;

import by.epam.jmp.persist.IEmployeeDao;
import by.epam.jmp.persist.entity.Employee;
import org.springframework.stereotype.Repository;

/**
 *  EmployeeDao implementation
 */
@Repository
public class EmployeeDaoImpl extends BaseDaoImpl<Employee> implements IEmployeeDao{

    protected Class<Employee> getEntityClass() {
        return Employee.class;
    }
}
