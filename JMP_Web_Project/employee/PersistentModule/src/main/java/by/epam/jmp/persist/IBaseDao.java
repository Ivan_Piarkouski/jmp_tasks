package by.epam.jmp.persist;

import java.util.List;

/**
 * Interface for Base dao, states CRUD methods
 */
public interface IBaseDao<T> {

    /**
     * saves given entity
     *
     * @param t - not Null
     */
    void save (T t);

    /**
     * updates given entity
     *
     * @param t - not Null
     * @return T t
     */
    T update (T t);

    /**
     * Deletes Entity with given Id
     *
     * @param id - not Null
     */
    void delete (Long id);

    /**
     * find entity with given Id
     *
     * @param id - not Null
     * @return T t
     */
    T find (Long id);

    /**
     * finds all entities
     *
     * @return  List<T>
     */
    List<T> findAll();
}
