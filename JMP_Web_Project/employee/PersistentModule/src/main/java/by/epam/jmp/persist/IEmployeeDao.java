package by.epam.jmp.persist;

import by.epam.jmp.persist.entity.Employee;

/**
 * Interface for Employee dao
 */
public interface IEmployeeDao extends IBaseDao <Employee> {
}
