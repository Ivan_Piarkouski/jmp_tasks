package by.epam.jmp.persist.impl;

import by.epam.jmp.persist.IBaseDao;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Base dao implementation
 */
@Repository
public abstract class BaseDaoImpl <T> implements IBaseDao<T> {

    @PersistenceContext
    private EntityManager entityManager;

    protected abstract Class<T> getEntityClass();

    /**
     * saves given entity
     *
     * @param t - not Null
     */
    public void save(T t) {
        entityManager.persist(t);
    }

    /**
     * updates given entity
     *
     * @param t - not Null
     * @return T t
     */
    public T update(T t) {
       return entityManager.merge(t);
    }

    /**
     * Deletes Entity with given Id
     *
     * @param id - not Null
     */
    public void delete(Long id) {
        entityManager.remove(find(id));
    }

    /**
     * find entity with given Id
     *
     * @param id - not Null
     * @return T t
     */
    public T find(Long id) {
        return entityManager.find(getEntityClass(), id);
    }

    /**
     * finds all entities
     *
     * @return List<T>
     */
    public List<T> findAll() {
       CriteriaQuery <T> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(getEntityClass());
       Root<T> variableRoot = criteriaQuery.from(getEntityClass());
       criteriaQuery.select(variableRoot);
       return entityManager.createQuery(criteriaQuery).getResultList();
    }
}
