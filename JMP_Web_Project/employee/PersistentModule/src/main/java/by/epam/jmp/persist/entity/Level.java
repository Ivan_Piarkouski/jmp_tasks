package by.epam.jmp.persist.entity;

/**
 * Enum for employee levels
 */
public enum Level {

    L1, L2, L3, L4
}
