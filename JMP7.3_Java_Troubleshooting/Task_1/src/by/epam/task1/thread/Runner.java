package by.epam.task1.thread;

import by.epam.task1.resource.Resource;

/**
 * Class extends Thread and used to implement it own logic of run method
 */
public class Runner extends Thread
{

    private Resource resource1;
    private Resource resource2;

    /**
     * Constructor with parameters for Runner class.
     *
     * @param name      - name of the Thread
     * @param resource1 - resource on which Runner tries to acquire first lock
     * @param resource2 - resource on which Runner tries to acquire secon lock
     */
    public Runner(String name, Resource resource1, Resource resource2)
    {
        super(name);
        this.resource1 = resource1;
        this.resource2 = resource2;
    }

    /**
     * method in which Runner first tries to acquire monitor on resource1 and than on resource2.
     * The resources are specified in constructor. Between acquiring first and second monitor
     * thread is put to sleep for 100ms to guarantee deadlock
     */
    @Override
    public void run()
    {
        synchronized (resource1)
        {
            System.out.println(getName()+" have taken monitor of " + resource1.getName());
            //thread is put to sleep for 100ms to give time for a second thread to acquire monitor
            try
            {
                sleep ( 100 );
            }
            catch ( InterruptedException ex ) {}

            synchronized (resource2)
            {
                System.out.println(getName()+" have taken monitor of" + resource2.getName());
            }
        }
    }
}
