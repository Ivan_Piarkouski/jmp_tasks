package by.epam.task1;

import by.epam.task1.resource.Resource;
import by.epam.task1.thread.Runner;

/**
 * Class starting the app
 */
public class Main {

    public static void main(String[] args)
    {
        Resource resource1 = new Resource("First resource");
        Resource resource2 = new Resource("Second resource");

        Runner runner1 = new Runner("runner1", resource1, resource2);
        Runner runner2 = new Runner("runner2", resource2, resource1);

        runner1.start();
        runner2.start();
    }
}
