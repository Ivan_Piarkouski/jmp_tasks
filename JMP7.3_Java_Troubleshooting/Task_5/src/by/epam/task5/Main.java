package by.epam.task5;

import by.epam.task5.reader.LineReader;

import java.io.IOException;
import java.util.List;

/**
 * Class starting the app
 */
public class Main {

    public static void main(String[] args) throws IOException
    {
        List<String> strings = LineReader.readTextFileToList(".\\resources\\Task #5 - Data.txt");
        System.out.println("List length: " + strings.size());
    }
}
