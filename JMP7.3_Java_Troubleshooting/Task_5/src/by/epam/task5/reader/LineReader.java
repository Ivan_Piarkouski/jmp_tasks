package by.epam.task5.reader;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for reading file line by line
 */
public class LineReader
{
    /**
     * Method reads data file line by line, take 3 first characters of every line
     * and put all the 3-character lines to ArrayList.
     *
     * @param filename - name of file
     * @return List<String> stringList
     * @throws IOException
     */
    public static List<String> readTextFileToList (String filename) throws IOException
    {
        List<String> stringList = new ArrayList<String>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(filename)));
        String line;
        while ((line = bufferedReader.readLine()) != null)
        {
            stringList.add(line.substring(0, 3));
        }
        return stringList;
    }
}
