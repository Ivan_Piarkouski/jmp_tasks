package by.epam.task4.resource;

/**
 * A class used to acquire lock on it
 */
public class Resource {

    private String name;

    /**
     * Constructor of Resource
     *
     * @param name - name of the Resource objecr
     */
    public Resource (String name)
    {
        this.name = name;
    }

    /**
     * method returns name of the Resource object
     *
     * @return name
     */
    public String getName(){
        return name;
    }
}
