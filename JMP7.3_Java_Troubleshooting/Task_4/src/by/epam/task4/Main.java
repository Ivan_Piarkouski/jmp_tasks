package by.epam.task4;

import by.epam.task4.resource.Resource;
import by.epam.task4.thread.Runner;

import java.util.ArrayList;
import java.util.List;

/**
 * Class which starts the app
 */
public class Main {

    public static void main(String[] args)
    {
	    Resource resource = new Resource("Common resource");
	    start(createListOfRunners(resource, 10));
    }

    /**
     * Method creates a list of runners initializing theirs name and common resource
     *
     * @param resource - resource used by all runners
     * @param runnerQuantity - quantity of runners to be created
     * @return List<Runner> runners
     */
    private static List<Runner> createListOfRunners(Resource resource, int runnerQuantity)
    {
        List<Runner> runners = new ArrayList<Runner>(runnerQuantity);
        for(int i=0; i<runnerQuantity; i++)
        {
            Runner runner = new Runner("runner" + i, resource);
            runners.add(runner);
        }
        return runners;
    }

    /**
     * method runs through list of runners and starts them
     * @param runners
     */
    private static void start(List<Runner> runners)
    {
        for(Runner runner : runners)
        {
            runner.start();
        }
    }
}
