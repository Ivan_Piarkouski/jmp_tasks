package by.epam.task4.thread;

import by.epam.task4.resource.Resource;

/**
 * Class extends Thread and used to implement it own logic of run method
 */
public class Runner extends Thread
{

    private Resource resource;

    /**
     * Constructor with parameters for Runner class.
     *
     * @param name      - name of the Thread
     * @param resource - resource on which Runner tries to acquire monitor
     */
    public Runner(String name, Resource resource)
    {
        super(name);
        this.resource = resource;
    }

    /**
     * method in which Runner first tries to acquire monitor on resource and than is put to sleep
     * for 1000ms to guarantee other Threads will wait
     */
    @Override
    public void run()
    {
        synchronized (resource)
        {
            System.out.println(getName()+" has taken monitor of " + resource.getName());
            //thread is put to sleep for 100ms to give time for a second thread to acquire monitor
            try
            {
                sleep ( 5000 );
            }
            catch ( InterruptedException ex ) {}
            System.out.println(getName()+" has released monitor of " + resource.getName());
        }
    }
}
