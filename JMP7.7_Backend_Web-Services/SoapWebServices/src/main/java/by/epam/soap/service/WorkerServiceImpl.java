package by.epam.soap.service;

import by.epam.soap.dao.WorkerDao;
import by.epam.soap.schema.Worker;

import javax.jws.WebService;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.soap.SOAPFaultException;
import java.util.List;

@WebService
public class WorkerServiceImpl
{
    /** workers dao */
    private WorkerDao workerDao = WorkerDao.getInstance();
    /** soap factory*/
    private SOAPFactory soapFactory;

    /**
     * constructor, initializes soap factory
     */
    public WorkerServiceImpl()
    {
        try
        {
            soapFactory = SOAPFactory.newInstance();
        }
        catch (SOAPException e)
        {
        }
    }

    /**
     * returns list of workers
     * @return workers
     */
    public List<Worker> getWorkers()
    {
        return workerDao.getAllWorkers();
    }

    /**
     * returns worker with specified id or throws SOAPException if not found
     * @param id - id
     * @return worker
     * @throws SOAPException
     */
    public Worker getWorker(Integer id) throws SOAPException{
        Worker worker = workerDao.getWorker(id);
        if (worker == null)
        {
            SOAPFault fault = soapFactory.createFault();
            fault.setFaultString("Worker not found");
            throw new SOAPFaultException(fault);
        }
        return worker;
    }

    /**
     * updates worker if updating worker not found throws SoapException
     * @param worker to update
     * @return result
     * @throws SOAPException
     */
    public String updateWorker(Worker worker) throws SOAPException
    {
        Worker oldWorker = workerDao.getWorker(worker.getId());
        String result;
        if(oldWorker != null)
        {
            workerDao.updateWorker(worker);
            result = "Success";
        }
        else
        {
            SOAPFault fault = soapFactory.createFault();
            fault.setFaultString("Worker not found");
            throw new SOAPFaultException(fault);
        }
        return result;
    }

    /**
     * adds specified worker
     * @param worker - worker to add
     * @return
     */
    public String addWorker(Worker worker)
    {
        workerDao.addWorker(worker);
        return "Success";
    }

    /**
     * deletes worker with specified id or throws SOAPException if not found
     * @param id - id
     * @return result
     * @throws SOAPException
     */
    public String deleteWorker(Integer id) throws SOAPException
    {
        String result;
        Worker deleteCandidate = workerDao.getWorker(id);
        if(deleteCandidate != null)
        {
            workerDao.deleteWorker(deleteCandidate);
            result = "Success";
        }
        else
        {
            SOAPFault fault = soapFactory.createFault();
            fault.setFaultString("Worker not found");
            throw new SOAPFaultException(fault);
        }
        return result;
    }
}