package by.epam.soap.dao;

import by.epam.soap.schema.Worker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A class containing workers
 */
public class WorkerDao {

    /** init names of workers */
    private final static String [] userNames = {"Ivan Spresov", "Ivan Perkovsky", "Pavel Urban", "Jury Bloshitsyn"};
    /** singleton instance */
    private final static WorkerDao instance = new WorkerDao();

    /** sequence to set id */
    private  Integer workersSequence;
    /** workers map */
    private Map<Integer, Worker> workers;

    /**
     * constructs worker dao
     */
    private WorkerDao()
    {
        workers = new HashMap<Integer, Worker>();
        for (int i = 0; i < userNames.length; i++) {
            Worker worker = new Worker(i, userNames[i]);
            workers.put(i, worker);
        }
        workersSequence = userNames.length - 1;
    }

    /**
     * returns reference to a singleton
     * @return instance
     */
    public static WorkerDao getInstance()
    {
        return instance;
    }

    /**
     * returns workers list
     * @return workers
     */
    public List<Worker> getAllWorkers()
    {
        return new ArrayList<Worker>(workers.values());
    }

    /**
     * returns worker with specified id or null
     * @param id workerId
     * @return worker
     */
    public Worker getWorker(Integer id)
    {
        return workers.get(id);
    }

    /**
     * updates given worker
     * @param worker worker
     */
    public synchronized void updateWorker(Worker worker)
    {
        workers.put(worker.getId(), worker);
    }

    /**
     * adds specified worker
     * @param worker worker
     */
    public synchronized void addWorker(Worker worker)
    {
        workersSequence = ++workersSequence;
        worker.setId(workersSequence);
        workers.put(workersSequence, worker);
    }

    /**
     * deletes specified worker
     * @param worker
     */
    public synchronized void deleteWorker (Worker worker)
    {
        workers.remove(worker.getId());
    }

}
