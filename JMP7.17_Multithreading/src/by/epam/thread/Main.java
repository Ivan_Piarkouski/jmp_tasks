package by.epam.thread;

import by.epam.thread.race.Race;
import org.apache.log4j.BasicConfigurator;

/**
 * class starts the app
 */
public class Main {

    // configuring log4j default settings
    static {
        BasicConfigurator.configure();
    }

    public static void main(String[] args) {
        Race race = new Race(100);
        race.startRace();
    }


}
