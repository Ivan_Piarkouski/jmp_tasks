package by.epam.thread.race;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Class which organizes the race
 */
public class Race {

    private Logger log = Logger.getLogger(getClass());
    private int carNumber;

    public Race(int carNumber) {
        this.carNumber = carNumber;
    }

    /**
     * method makes all necessary preparation and starts the race
     */
    public void startRace() {
        Finish finish = new Finish();
        List<Car> cars = prepareCars(carNumber, finish);
        startRaceInDifferentThreads(cars);
        log.info("The Winner is " + finish.getCars().get(0).getName());

    }

    /**
     * Prepares the race participants
     *
     * @param carNumber - 1 or more
     * @param finish - not null
     * @return List<Car> cars
     */
    private static List<Car> prepareCars(int carNumber, Finish finish) {
        if (carNumber < 1) {
            throw new IllegalArgumentException("carNumber must be 1 or more");
        }
        List<Car> cars = new ArrayList<>(carNumber);
        //randomly generates number of cheater, who will be disqualified
        int cheaterNumber = ThreadLocalRandom.current().nextInt(0, carNumber + 1);

        for (int i = 1; i < carNumber; i++) {
            Car car;
            //randomly generates friction
            long friction = ThreadLocalRandom.current().nextLong(50, 101);
            if (i != cheaterNumber) {
                car = new Car("car" + i, friction, finish, false);
            } else {
                car = new Car("car" + i, friction, finish, true);
            }
            cars.add(car);
        }
        return cars;
    }

    /**
     * starts the racing (runs threads) and join them to the main thread
     *
     * @param cars - not null
     * @return List<Thread> racingCars
     */
    private List<Thread> startRaceInDifferentThreads(List<Car> cars) {
        List<Thread> racingCars = new ArrayList<>();
        //starts threads
        for (Car car : cars) {
            Thread thread = new Thread(car);
            racingCars.add(thread);
            thread.start();
        }
        //joins threads to main in order the main thread waited, till the race finishes, before announcing the winner
        for (Thread thread : racingCars) {
            try {
                thread.join();
            } catch (InterruptedException e) {
               log.error("thread: "+ thread.getName() + " failed to join", e);
            }
        }
        return racingCars;
    }
}
