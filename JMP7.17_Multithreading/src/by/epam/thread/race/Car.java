package by.epam.thread.race;

import org.apache.log4j.Logger;

/**
 * Car
 */
public class Car implements Runnable {

    private Finish finish;
    private static final long MAX_DISTANCE = 10000;
    private long friction;
    private long distance;
    private String name;
    private boolean cheater;
    private Logger log = Logger.getLogger(getClass());

    /**
     * Constructor
     *
     * @param name
     * @param friction
     * @param finish
     * @param cheater
     */
    public Car(String name, long friction, Finish finish, boolean cheater) {
        this.name = name;
        this.friction = friction;
        this.finish = finish;
        this.cheater = cheater;
    }

    /**
     * returns car name
     *
     * @return String name
     */
    public String getName() {
        return name;
    }

    @Override
    public void run() {
        // if car is a cheater - creates and runs a thread,  which in 5 secs interrupts car
        if (cheater) {
            Thread cheaterThread = Thread.currentThread();
            Thread thread = new Thread(() -> {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    log.error("Disqualification failed", e);
                }
                cheaterThread.interrupt();
            });
            thread.start();
        }
        try {
            while (distance < MAX_DISTANCE) {
                Thread.sleep(friction);
                distance += 100;
                log.info(name + " " + distance);
            }
            finish.finish(this);
        } catch (InterruptedException e) {
            log.error("Car " + name + " was interrupted", e);
        }
    }

}
