package by.epam.thread.race;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Finish, only one car can finish at once
 */
public class Finish {

    private Logger log = Logger.getLogger(getClass());
    private ReentrantLock lock = new ReentrantLock();
    private List<Car> cars = new ArrayList<>();

    /**
     * finishes a car
     *
     * @param car - not null
     */
    public void finish(Car car){
        if (car == null){
            throw new IllegalArgumentException("Car can't be null");
        }
        lock.lock();
        try {
            log.info("Car " + car.getName() + " finishes");
            cars.add(car);
        }
        finally {
            lock.unlock();
        }
    }

    /**
     * returns a list of finished cars
     *
     * @return List<Car> cars
     */
    public List<Car> getCars() {
        return cars;
    }
}
