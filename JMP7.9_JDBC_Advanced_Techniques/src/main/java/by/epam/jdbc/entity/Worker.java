package by.epam.jdbc.entity;

import java.io.Serializable;

/**
 * Worker resource for REST manipulation
 */
public class Worker implements Serializable{

    private static final long serialVersionUID = 1L;
    private Integer id;
    private String name;
    private Integer level;
    private Integer salary;

    public Worker()
    {}

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Integer getLevel()
    {
        return level;
    }

    public void setLevel(Integer level)
    {
        this.level = level;
    }

    public Integer getSalary()
    {
        return salary;
    }

    public void setSalary(Integer salary)
    {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Worker worker = (Worker) o;

        if (id != null ? !id.equals(worker.id) : worker.id != null) return false;
        if (name != null ? !name.equals(worker.name) : worker.name != null) return false;
        if (level != null ? !level.equals(worker.level) : worker.level != null) return false;
        return salary != null ? salary.equals(worker.salary) : worker.salary == null;
    }

    @Override
    public int hashCode()
    {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (level != null ? level.hashCode() : 0);
        result = 31 * result + (salary != null ? salary.hashCode() : 0);
        return result;
    }

    @Override
    public String toString()
    {
        return "Worker{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", level=" + level +
                ", salary=" + salary +
                '}';
    }
}
