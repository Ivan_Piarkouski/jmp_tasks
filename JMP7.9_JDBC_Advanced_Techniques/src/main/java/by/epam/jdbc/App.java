package by.epam.jdbc;

import by.epam.jdbc.dao.WorkerDao;
import by.epam.jdbc.entity.Worker;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Hello world!
 *
 */
public class App
{
    final static Logger logger = Logger.getLogger(App.class);

    private static void printWorkers(List<Worker> workers)
    {
        for(Worker worker: workers)
        {
            logger.info(worker);
        }
    }

    public static void main( String[] args )
    {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        WorkerDao workerDao = context.getBean("WorkerDao", WorkerDao.class);

        List<Worker> workers = workerDao.getAllWorkers();
        Integer workerId = workers.get(workers.size()-1).getId();

        Worker workerToUpdate = workerDao.getWorker(workerId);
        logger.info(workerToUpdate);
        workerToUpdate.setSalary(100);
        workerToUpdate.setName("Looser");
        logger.info("Number of workers updated: "+workerDao.updateWorker(workerToUpdate));
        logger.info(workerDao.getWorker(workerId));

        printWorkers(workerDao.getAllWorkers());
        Worker workerToDel = new Worker();
        workerToDel.setId(workerId);
        logger.info("Number of workers deleted: "+workerDao.deleteWorker(workerToDel));
        printWorkers(workerDao.getAllWorkers());

        Worker workerToAdd = new Worker();
        workerToAdd.setName("SUPERMANN");
        workerToAdd.setLevel(100);
        workerToAdd.setSalary(10000000);
        logger.info("Number of workers added: "+workerDao.addWorker(workerToAdd));
        printWorkers(workerDao.getAllWorkers());

    }
}
