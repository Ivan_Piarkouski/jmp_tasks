package by.epam.jdbc.dao;



import by.epam.jdbc.entity.Worker;

import java.util.List;

/**
 * Interface for Dao
 */
public interface WorkerDao
{
    /**
     * returns workers list
     * @return workers
     */
    List<Worker> getAllWorkers();

    /**
     * returns worker with specified id or null
     * @param id workerId
     * @return worker
     */
    Worker getWorker(Integer id);

    /**
     * updates given worker
     * @param worker worker
     * @return number of rows affected
     */
    int updateWorker(Worker worker);

    /**
     * adds specified worker
     * @param worker worker
     * @return number of rows affected
     */
    int addWorker(Worker worker);

    /**
     * deletes specified worker
     * @param worker
     * @return number of rows affected
     */
    int deleteWorker(Worker worker);
}
