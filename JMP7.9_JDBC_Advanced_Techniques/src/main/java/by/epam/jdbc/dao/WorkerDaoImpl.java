package by.epam.jdbc.dao;


import by.epam.jdbc.entity.Worker;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * A class containing workers
 */
public class WorkerDaoImpl implements WorkerDao
{
    /** JDBC template */
    private JdbcTemplate jdbcTemplate;

    private final static String SQL_SELECT_ALL = "SELECT * FROM workers";
    private final static String SQL_SELECT_WORKER = "SELECT * FROM workers WHERE worker_id=?";
    private final static String SQL_UPDATE_WORKER = "UPDATE workers SET name=?, level=?, salary=? "
            + "WHERE worker_id=?";
    private final static String SQL_INSERT_WORKER = "INSERT INTO workers (name, level, salary)"
            + " VALUES (?, ?, ?)";
    private final static String SQL_DELETE_WORKER = "DELETE FROM workers WHERE worker_id=?";

    private final static String COLUMN_WORKER_ID = "worker_id";
    private final static String COLUMN_NAME = "name";
    private final static String COLUMN_LEVEL = "level";
    private final static String COLUMN_SALARY = "salary";

    /**
     * constructs worker dao
     */
    public WorkerDaoImpl()
    {}

    /**
     * constructs worker dao
     */
    public WorkerDaoImpl(DataSource dataSource)
    {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /**
     * creates worker using result set
     *
     * @param rs - result set
     * @return worker
     * @throws SQLException
     */
    private Worker createWorker(ResultSet rs) throws SQLException
    {
        Worker worker = new Worker();

        worker.setId(rs.getInt(COLUMN_WORKER_ID));
        worker.setName(rs.getString(COLUMN_NAME));
        worker.setLevel(rs.getInt(COLUMN_LEVEL));
        worker.setSalary(rs.getInt(COLUMN_SALARY));

        return worker;
    }

    /**
     * returns workers list
     * @return workers
     */
    public List<Worker> getAllWorkers()
    {
        List<Worker> workers = jdbcTemplate.query(SQL_SELECT_ALL, new RowMapper<Worker>() {

            @Override
            public Worker mapRow(ResultSet rs, int rowNum) throws SQLException {
                return createWorker(rs);
            }
        });

        return workers;
    }

    /**
     * returns worker with specified id or null
     * @param id workerId
     * @return worker
     */
    public Worker getWorker(Integer id)
    {
        return jdbcTemplate.query(SQL_SELECT_WORKER, new ResultSetExtractor<Worker>() {

            @Override
            public Worker extractData(ResultSet rs) throws SQLException,
                    DataAccessException {
                if (rs.next()) {
                    return createWorker(rs);
                }
                return null;
            }
        }, id);
    }

    /**
     * updates given worker
     * @param worker worker
     * @return number of rows affected
     */
    public int updateWorker(Worker worker)
    {
        return jdbcTemplate.update(SQL_UPDATE_WORKER, worker.getName(), worker.getLevel(),
                worker.getSalary(), worker.getId());
    }

    /**
     * adds specified worker
     * @param worker worker
     * @return number of rows affected
     */
    public int addWorker(Worker worker)
    {
        return jdbcTemplate.update(SQL_INSERT_WORKER, worker.getName(), worker.getLevel(),
                worker.getSalary());
    }

    /**
     * deletes specified worker
     * @param worker
     * @return number of rows affected
     */
    public int deleteWorker (Worker worker)
    {
        return jdbcTemplate.update(SQL_DELETE_WORKER, worker.getId());
    }

}
