package by.epam.task4.animal;

/**
 * Class for animals
 */
public interface Animal {

    /**
     * method play
     */
    void play();

    /**
     * method voice
     */
    void voice();
}
