package by.epam.task4.loader;

import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Custom classLoader to load classes from jar
 */
public class CustomClassLoader extends ClassLoader {

    /** path tom jar */
    private String jarFilePath;

    /** logger */
    private Logger logger = Logger.getLogger(CustomClassLoader.class);

    /**
     * Constructor of class loader
     *
     * @param jarFilePath - path tom jar
     */
    public CustomClassLoader(String jarFilePath)
    {
        super(CustomClassLoader.class.getClassLoader());
        this.jarFilePath = jarFilePath;
    }

    /**
     * method closes JarFile, catches IOException and logs it
     * @param jar - JArFile
     */
    private void closeJar(JarFile jar)
    {
        if(jar != null)
        {
            try
            {
                jar.close();
            }
            catch (IOException ex)
            {
                logger.error("Unable to close jar file", ex);
            }
        }
    }

    /**
     * Method tries to find the specified jar and load class from it
     *
     * @param className - name of loaded class, not null
     * @return loadedClass
     * @throws ClassNotFoundException in case of problems
     */
    @Override
    protected Class<?> findClass(String className) throws ClassNotFoundException
    {
        JarFile jar = null;
        try {
            jar = new JarFile(jarFilePath);
            JarEntry entry = jar.getJarEntry(className.replace('.', '/') + ".class");
            if(entry == null)
            {
                throw new ClassNotFoundException("No class with name \"" + className + "\" in jar \"" +
                        jarFilePath + "\"");
            }
            InputStream iStream = jar.getInputStream(entry);
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            int nextValue = iStream.read();
            while (-1 != nextValue)
            {
                byteStream.write(nextValue);
                nextValue = iStream.read();
            }

            byte classByte[] = byteStream.toByteArray();
            //if class is not found - defineClass throws ClassFormatError, so there is no need to check for null
            return defineClass(className, classByte, 0, classByte.length, null);
        }
        catch (IOException e)
        {
            throw new ClassNotFoundException ("Cannot load class from jar located at thr following jarPath: " +
                    jarFilePath, e);
        }
        finally
        {
            closeJar(jar);
        }
    }
}
