package by.epam.task4;


import by.epam.task4.animal.Animal;
import by.epam.task4.loader.CustomClassLoader;
import org.apache.log4j.BasicConfigurator;

import java.util.ArrayList;
import java.util.List;

/**
 * Class starting the app
 */
public class Main {

    // configuring log4j default settings
    static
    {
        BasicConfigurator.configure();
    }

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException,
            InstantiationException
    {
        List<Class<?>> animalsClasses = new ArrayList<Class<?>>(2);
        CustomClassLoader customClassLoader =
                new CustomClassLoader("..\\AnimalsImpl\\jar\\animalsImpl.jar");

        animalsClasses.add(customClassLoader.loadClass("by.epam.task4.animal.impl.Cat"));
        animalsClasses.add(customClassLoader.loadClass("by.epam.task4.animal.impl.Dog"));

        for (Class<?> animalClass : animalsClasses)
        {
            Animal animal = (Animal) animalClass.newInstance();
            animal.voice();
            animal.play();
        }
    }
}
