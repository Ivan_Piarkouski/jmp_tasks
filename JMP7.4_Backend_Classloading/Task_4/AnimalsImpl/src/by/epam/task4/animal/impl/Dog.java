package by.epam.task4.animal.impl;

import by.epam.task4.animal.Animal;
import org.apache.log4j.Logger;

/**
 * class Dog implements Animal
 */
public class Dog implements Animal
{

    private Logger logger = Logger.getLogger(Dog.class);

    @Override
    public void play()
    {
        logger.info("Dog is playing");
    }

    @Override
    public void voice()
    {
        logger.info("Gau");
    }
}
