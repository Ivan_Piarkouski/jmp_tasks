package by.epam.task4.animal.impl;

import by.epam.task4.animal.Animal;
import org.apache.log4j.Logger;

/**
 * class Cat implements Animal
 */
public class Cat implements Animal
{

    private Logger logger = Logger.getLogger(Cat.class);

    @Override
    public void play()
    {
        logger.info("Cat is playing");
    }

    @Override
    public void voice()
    {
        logger.info("Mey");
    }
}
